var gulp = require('gulp')
  , gutil = require('gulp-util')
  , less = require('gulp-less')
  , sourcemaps = require('gulp-sourcemaps')
  , inject = require('gulp-inject')
  , argv = require('yargs').argv
  , connect = require('connect')
  , serveStatic = require('serve-static')
  , connectLr = require('connect-livereload')
  , tinyLr = require('tiny-lr')
  , request = require('request')
  , Q = require('q')
  , browserify = require('browserify')
  , source = require('vinyl-source-stream')
  , buffer = require('vinyl-buffer')
  ;

var config = {
  app: {
    src: 'static/',
    dist: 'dist/',
  },
  less: {
    src: [
      '{{ config.app.src }}/less/main.less'
    ],
    dest: '{{ config.app.src }}/css/',
    watch: [
      '{{ config.app.src }}/less/**'
    ],
    config: {
      paths: [
        '{{ config.app.src }}/less/',
        '{{ config.app.src }}/vendors/',
        '{{ config.app.src }}'
      ]
    }
  },
  js: {
    src: [
      '{{ config.app.src }}/react/main.js'
    ],
    dest: '{{ config.app.src }}/js/',
    watch: [
      '{{ config.app.src }}/react/**'
    ],
    config: {
      entries: '{{ config.app.src }}/react/main.js',
      debug: true,
      transform: [
        ["babelify", {presets: ['react']}]
      ]
    }
  },
};

// Yep, need to interpolate before use
config = interpolate(config);

gulp.task('default', ['dev.less', 'dev.js', 'dev.watch']);

gulp.task('dev.watch', function() {
  gulp.watch(config.less.watch, ['dev.less']);
  gulp.watch(config.js.watch, ['dev.js']);
});

gulp.task('dev.less', function() {
  gulp.src(config.less.src)
  .pipe(sourcemaps.init())
  .pipe(less(config.less.config))
  .pipe(sourcemaps.write())
  .pipe(gulp.dest(config.less.dest));
});

gulp.task('dev.js', function() {
  var b = browserify(config.js.config);

  return b.bundle()
          .pipe(source('main.js'))
          .pipe(buffer())
          .pipe(gulp.dest(config.js.dest));
});

gulp.task('dist', ['dev.less', 'dev.js'], function() {

  // DEV
  gulp
    .src(['./**',
      /*
      '!'+config.app.src+'/less/**',
      '!'+config.app.src+'/less',
      '!'+config.app.src+'/react/**',
      '!'+config.app.src+'/react',
      '!./bower.json',
      '!./gulpfile.js',
      */
      '!./node_modules/**',
      '!./node_modules',
      '!'+config.app.src+'/vendors/**',
      '!'+config.app.src+'/vendors'])
    .pipe(gulp.dest(config.app.dist));

});

/**
 * @param opt
 * @return {Object}
 * @description
 */
function interpolate(opt) {
  var self = this;

  if('string' == typeof opt && /{{.+?}}/.test(opt)) {
    opt = opt.replace(/{{\s*(.+?)\s*}}/g, function(str, expr) {
      return eval(expr);
    });
  } else if('object' == typeof opt) {
    for(var k in opt) {
      opt[k] = arguments.callee.call(self, opt[k]);
    }
  }

  return opt;
}

