/*
 * This application is an exam
 * I don't have many time to do it.
 * So the UX maybe not good
 *
 * Contact me if you interesting my codes
 */

var React = require('react')
  , ReactDOM = require('react-dom')
  , $ = require('jquery')
  , classNames = require('classnames')
  , Config = {
    isViewGrid: true,
    curency: '$',
    itemsPerPage: 30,
    bottomSpacing: 50,
    adsPerItems: 20,
    message: {
      loading: "loading..",
      limited: "~ end of catalogue ~"
    },
  }

  // Define object using on React elements
  , SortButton = React.createClass({
    sortSize: function() {
      this.props.handleClick('size');
      return false;
    },
    sortPrice: function() {
      this.props.handleClick('price');
      return false;
    },
    sortId: function() {
      this.props.handleClick('id');
      return false;
    },
    render: function() {
      return (
        <div className="product-sort">
          Filter by:
          <a href="#" onClick={this.sortSize}>Size</a>
          <a href="#" onClick={this.sortPrice}>Price</a>
          <a href="#" onClick={this.sortId}>Id</a>
        </div>
      )
    }
  })
  , ViewSelect = React.createClass({
    render: function() {
      return (
        <div className="view-select">
          <button className="btn-grid" type="button" onClick={this.props.viewGrid}><span></span></button>
          <button className="btn-list" type="button" onClick={this.props.viewList}><span></span></button>
        </div>
      )
    }
  })
  , Ads = React.createClass({
    render: function() {
      return (
        <div className="item">
          <img className="ad" src="/ad/?r=' + Math.floor(Math.random()*1000) + '"/>
        </div>
      )
    }
  })
  , Message = React.createClass({
    render: function() {
      return (
        <div className={"message " + this.props.classes}>
          <span>{this.props.msg}</span>
        </div>
      )
    }
  })
  ;

// Main object
var ProductTable = React.createClass({

  /**
   * Initital data with react state
   * @return react state
   */
  getInitialState: function() {
    return {
      data: [],
      dataRendered: [],
      apiUrl: '/api/products?',
      isViewGrid: Config.isViewGrid,
      curency: Config.curency,
      isLoading: false,
      currentPage: -1,
      currentActivePage: -1,
      isLimitedProduct: false,
      isRenderWaiting: false,
      message: Config.message.loading,

      sort: '',
      adsIndex: 0
    };
  },

  componentDidMount: function() {
    this.registerEvent();
    this.nextPage();
  },

  /**
   * Register global event: scroll to bottom
   */
  registerEvent: function() {
    var _self = this;
    // handle event
    window.addEventListener("optimizedScroll", function() {
      var doc = document.documentElement
        , top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0)
        , windowHeight = window.innerHeight
          || document.documentElement.clientHeight
          || document.body.clientHeight
        , docHeight = getDocHeight()
      ;
      if( (docHeight - (top+windowHeight) ) < Config.bottomSpacing) {
        _self.nextPage();
      }
    });
  },

  /*
  * Render next page
  * @return
  * @param
  * @description this function render data when user scroll to bottom of page
  */
  nextPage: function() {
    this.renderActivePage();
    this.getNextPageData();
  },

  /**
   * Fetch data when user view the products
   */
  getNextPageData: function() {
    var _self = this;
    if(!this.state.isLimitedProduct && !this.state.isLoading) {
      this.loadProducts(function() {
        _self.setState({ currentPage: _self.state.currentPage + 1});

        //Force render when current page = 0
        if(_self.state.currentPage == 0) {
          _self.nextPage();
        } else if(_self.state.isRenderWaiting) {
          // Check if waiting for render
          _self.renderActivePage();
        }
      });
    }
  },

  /**
   * Render data when user scroll to the bottom
   * also add Image Ads to the grid of products
   */
  renderActivePage: function() {
    var _self = this;
    if(this.state.currentActivePage < this.state.currentPage) {
      this.setState({ currentActivePage: this.state.currentActivePage + 1});
      var rendered = this.state.dataRendered;
      var pageData = this.state.data[this.state.currentActivePage];
      if(pageData != undefined) {

        //Every page we need to add the Image Ads
        var adsIndex = this.state.adsIndex + Config.adsPerItems;
        var len = pageData.length;
        for(var i in pageData) {
          if(i == adsIndex) {
            rendered.push({
              id: 'lady-' + this.state.currentActivePage + '-' + adsIndex,
              type: 'ads',
              src: '/ad/?r=' + Math.floor(Math.random()*1000)
            });
            if(adsIndex+Config.adsPerItems < len) {
              adsIndex = adsIndex+Config.adsPerItems;
            }
          }

          //add data to rendered object
          rendered.push(pageData[i]);
        }

        // if len == adsIndex
        if(len == adsIndex) {
          rendered.push({
            id: 'lady-' + this.state.currentActivePage + '-' + adsIndex,
            type: 'ads',
            src: '/ad/?r=' + Math.floor(Math.random()*1000)
          });
        }

        // reset ads index
        adsIndex = adsIndex - len;
        this.setState({ isLoading: true , adsIndex: adsIndex }, function() {
          _self.setState({ dataRendered: rendered }, function() {
            _self.setState({ isLoading: false });
          });
        });
      }

      this.setState({ isRenderWaiting: false });
    } else if(!this.state.isLimitedProduct) {
      this.setState({ isRenderWaiting: true });
    } else {
      // Limited product
      this.setState({ message: Config.message.limited });
    }
  },
  loadProducts: function(onSuccess, onError) {
    var _self = this;

    //Get the url
    var url = this.state.apiUrl+'limit='+Config.itemsPerPage;
    url = url+ '&skip=' + (Config.itemsPerPage * ( this.state.currentPage + 1));
    var sort = this.state.sort.trim();
    if(sort != '') {
      url = url + '&sort=' + sort;
    }

    // And call the ajax via Jquery Ajax
    $.ajax({
      url: url,
      dataType: 'text',
      cache: false,
      success: function(data) {
        //convert response to Json string
        data = "[" + data.replace(/\n\{/g, ", \{") + "]";
        var obj = JSON.parse(data);
        if(obj.length > 0) {
          var cur = this.state.data;
          cur.push(obj);
          this.setState({data: cur, isLoading: false});
        } else {
          this.setState({isLimitedProduct: true, isLoading: false});
        }
        if(onSuccess != undefined) onSuccess();
      }.bind(this),
      error: function(xhr, status, err) {
        this.setState({isLoading: false});
        console.error(url, status, err.toString());
      }.bind(this),
      beforeSend: function() {
        _self.setState({isLoading: true});
      },
      complete: function() {
      //  _self.setState({isLoading: false});
      }
    });
  },

  /**
   * Sort the products
   * @param: price, type, id  String
   * @return
   */
  sortData:function(type) {
    var _self = this;

    // Stop order when other ajax on calling
    if(this.state.isLoading) {
      return;
    }

    if(type != this.state.sort) {
      //Ignore first time
      if(this.state.dataRendered.length > 0) {
        this.resetProductList(function() {
          _self.setState({ sort: type }, _self.nextPage);
        });
      }
    }

  },

  /**
   * Why we need reset everything?
   * Because when user click on the sort
   * Every param need to be refresh
   */
  resetProductList: function(callback) {
    this.setState({
      data : [],
      dataRendered: [],

      curency: Config.curency,
      isLoading: false,
      currentPage: -1,
      currentActivePage: -1,
      isLimitedProduct: false,
      isRenderWaiting: false,
      message: Config.message.loading,

      sort: '',
      adsIndex: 0
    }, callback);
  },

  /**
   * Render data to view
   */
  render: function() {
    var _self = this
      , classes = classNames('row', {
          'view-grid': this.state.isViewGrid
        })
      , loadingClasses = classNames({
          'show': this.state.isLoading || this.state.isLimitedProduct
        })
      , message = this.state.message
      , curency = this.state.curency
      , index = 0
      ;

    var ProductNodes = this.state.dataRendered.map(function(p) {
      var date = new Date(p.date);
      var d = _self.timeString(date);

      // Render ADS
      if(p.type == 'ads') {
        //do the ads right here
        return (
          <div className="column" key={p.id}>
            <div className="lady-item">
              <img src={p.src} alt="" />
            </div>
          </div>
        )
      }

      // Render product item
      return (
        <div className="column" key={p.id}>
          <div className="item">
            <div className="face" style={{fontSize: p.size + 'px'}} >{p.face}</div>
            <div className="caption">
              <div className="size">size: {p.size}</div>
              <div className="price">{curency}{p.price}</div>
              <div className="date">{d}</div>
            </div>
          </div>
        </div>
      )
    });

    return (
      <div className="view">
        <div className="product-header">
          <ViewSelect viewGrid={this.viewGrid} viewList={this.viewList}></ViewSelect>
          <SortButton handleClick={this.sortData}></SortButton>
        </div>
        <div className={ classes }>
          {ProductNodes}
        </div>
        <Message msg={message} classes={ loadingClasses }></Message>
      </div>
    );
  },

  /**
   * Display product in grid
   * Using css only
   */
  viewGrid: function() {
    this.setState({ isViewGrid : true });
  },

  /**
   * Display product in list
   * Using css only
   */
  viewList: function() {
    this.setState({ isViewGrid : false });
  },

  /**
   * Get string time
   * ex: two day ago, 22-12-2015
   * @param: date Date
   * @return timeString String
   */
  timeString: function(date) {
    var seconds = Math.floor((new Date() - date) / 1000);
    var interval = Math.floor(seconds / 604800);
    if (interval >= 1) {
      return date.toLocaleFormat('%d-%m-%Y');
    }
    interval = Math.floor(seconds / 86400);
    if (interval >= 1) {
      return interval + " days ago";
    }
    interval = Math.floor(seconds / 3600);
    if (interval >= 1) {
      return interval + " hours ago";
    }
    interval = Math.floor(seconds / 60);
    if (interval >= 1) {
      return interval + " minutes ago";
    }
    if (interval > 4) {
      return Math.floor(seconds) + " seconds ago";
    }

    return 'Just now';
  }

});

ReactDOM.render(
  <ProductTable/>,
  document.getElementsByClassName('products')[0]
);






// 3rd library
// Add custom event scroll
;(function() {
    var throttle = function(type, name, obj) {
        obj = obj || window;
        var running = false;
        var func = function() {
            if (running) { return; }
            running = true;
            requestAnimationFrame(function() {
                obj.dispatchEvent(new CustomEvent(name));
                running = false;
            });
        };
        obj.addEventListener(type, func);
    };

    /* init - you can init any event */
    throttle ("scroll", "optimizedScroll");
})();

// Get document height
function getDocHeight() {
  var D = document;
  return Math.max(
    D.body.scrollHeight, D.documentElement.scrollHeight,
    D.body.offsetHeight, D.documentElement.offsetHeight,
    D.body.clientHeight, D.documentElement.clientHeight
  );
}

if (!Date.prototype.toLocaleFormat) {
  (function() {
    Date.prototype.toLocaleFormat = Date.prototype.toLocaleFormat || function(pattern) {
      return pattern.replace(/%Y/g, this.getFullYear()).replace(/%m/g, (this.getMonth() + 1)).replace(/%d/g, this.getDate());
    };
  }());
}
